from flask import Flask, render_template, request
from os.path import exists
import logging
import re
app = Flask(__name__)

@app.route("/<template>")
def templaterender(template):
    url = re.sub("https://", "", request.url)
    url = re.sub("http://", "", url)
    if ".." in url or "~" in url or "./" in url or "//" in url:
        return render_template("403.html"), 403
    else: 
        if exists("./templates" + request.path):
            return render_template(template), 200
        else:
            return render_template("404.html"), 404

    return "fuck"

@app.errorhandler(404)
def not_found(e):
    """Page not found."""
    return render_template("404.html"),404

@app.errorhandler(403)
def forbidden(e):
    """Page not found."""
    return render_template("403.html"),403

@app.route("/")
def index():
    url = re.sub("https://", "", request.url)
    url = re.sub("http://", "", url)
    if ".." in url or "~" in url or "./" in url or "//" in url:
        return render_template("403.html"), 403
    else:
        return render_template("404.html"), 404
   

if __name__ == "__main__":
    app.run(port=5000, debug=True,host='0.0.0.0')